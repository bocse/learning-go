

*Done steps*


Go Slices (instead of arrays)
https://blog.golang.org/go-slices-usage-and-internals

Waiting for threads (go routines)
https://nathanleclaire.com/blog/2014/02/15/how-to-wait-for-all-goroutines-to-finish-executing-before-continuing/


*To Do Steps*
Multithreading
https://pragmacoders.com/blog/multithreading-in-go-a-tutorial

Tour
https://tour.golang.org/basics/16

Documenting Go code
https://blog.golang.org/godoc-documenting-go-code

Organizing Go code
https://blog.golang.org/organizing-go-code


*Concepts to set up*

Logging
{
    non-locking
    async
    persistent
    rotating
}

Math and Complex Math {}

Images {}

HTTP {}

Building OpenAPI v3 into Go code {}

Building OpenAPI v3 into Protobuf {}

Building Protobuf into Go code {}

