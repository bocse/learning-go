package main

import (
	"fmt"
	"math/big"
	"math/cmplx"
	"math/rand"
	"time"
)

func complexCalculation(zin complex128) complex128 {
	start := time.Now()
	var ToBe bool = false
	var MaxInt uint64 = 1<<64 - 1
	var zout complex128 = cmplx.Sqrt(zin)

	fmt.Printf("Inthread Type: %T Value: %v\n", ToBe, ToBe)
	fmt.Printf("Inthread Type: %T Value: %v\n", MaxInt, MaxInt)
	fmt.Printf("Inthread Type: %T Value: %v\n", zout, zout)

	//The binomial calculation below should take long
	r := new(big.Int)
	fmt.Println(r.Binomial(1000, 10))

	elapsed := time.Since(start)
	fmt.Printf("Inthread Type: %T Latency: %v\n", elapsed, elapsed)
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Nanosecond)

	return zout
}
func needFloat(x float64) float64 {
	return x * 0.1
}

func main() {
	inputNumbers := []complex128{3 + 5i, -3 + 2i, -3, 5 - 3i}
	for i, c := range inputNumbers {
		fmt.Printf("Before thread start - >Type: %T Value: %v\n", i, i)
		go complexCalculation(c)
		fmt.Printf("After thread start - >Type: %T Value: %v\n", i, i)
	}
	time.Sleep(time.Second * 3)
}
